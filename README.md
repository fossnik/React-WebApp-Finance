# Yz (wise) Finance Project

### [JavaScript Frameworks](#javascript-frameworks-1)
- Node.js
- Express.js
- React.js & Redux.js
- TypeScript
- React Router

### [Web Scraping](#web-scraping-1)
- Yahoo Finance
- Java Web Scraper (jsoup)

### [Database](#database-1)
- SQLite
- SQLite-jdbc
- node-sqlite3

### [Hosting Environment](#hosting-environment-1)
- Linode
- Arch Linux
- Security / Server Hardening
- Domain Name Registration

## JavaScript Frameworks
### [Node.js](https://www.nodejs.org)
`Node.JS`  is a cross-platform JavaScript run-time environment which has become increasingly ubiquitous in the web-development sphere. The [npm ecosystem](https://www.npmjs.com/) provides access to a full gamut of high-quality software extensions for virtually any use case.

### [Express.js](https://www.reactjs.org)
`Express.js` is an event-driven HTTP framework often used for API. I use two independent Express instances - the frontend instance is a React website (yzFinance), which issues HTTP GET requests to the backend API (which itself is an Express.js instance with the ability to query the SQLite DB where my jSoup web-scrape data is persisted.) 

### [React.js](https://www.reactjs.org)
`React.js`  is a framework created by Facebook to facilitate more responsive UI; efficient state-driven web applications that spare server-side resources by offloading tasks to the client-side JavaScript interpreter.
`JSX Syntax`  is JavaScript syntax used to write React components.  It conveniently resembles the structure of HTML, but is not HTML; in fact, it is a shorthand for vanilla JavaScript functions that are significantly more verbose.

### [Redux.js](https://redux.js.org)
`Redux.js` - state management engine commonly paired with React.js.

### [React Router](https://reacttraining.com/react-router)
`React Router` is a Node.js package that implements navigation components and routing through a declarative programming model.
This allows a unique URL to be associated to each object in my dataset.

### [TypeScript](https://www.typescriptlang.org)
`TypeScript` extends the JavaScript language, making it possible to explicitly define the shape of objects when they are declared, or passed into / returned from a function.
Using TypeScript allowed me to define highly specific data objects and validate the construction of data passing within the program for total consistency. 

## Web Scraping
### [YahooFinance](https://finance.yahoo.com)
`Yahoo! Finance` is a free web resource and financial portfolio service from which the contents of the SQLite database are derived. It is public and reliable, although the implementation has tended to mutate on occasion. `finance.yahoo.com/cryptocurrencies`

### [jsoup](https://jsoup.org)
This project had originally used Selenium to perform web-scraping, but due to some difficulties with automating the chrome web driver to run headlessly on my Linode VM, it became clear that something simpler was preferable. Whereas Selenium is generally better suited to web testing automation because it emulates a full-fledged web browser, jsoup is extremely useful for simple data extraction.
The [jsoup webscraper is available on GitLab](https://gitlab.com/fossnik/JsoupWebScraper-yahFin).

## Database
### [SQLite](https://www.sqlite.org)
`SQLite` is an open-source relational database management system that is one of most popular among a myriad of competing SQL-style database products. Although these are largely interchangeable, SQLite does appear to be a favorite in the world of Java and JavaScript because of it's active development, strong user-base, and vital support community.

### [SQLite-jdbc](http://www.sqlitetutorial.net/sqlite-java/sqlite-jdbc-driver)
The `SQLite-jdbc driver` for the Java Database Connector (JDBC) interfaces the Selenium web-scraper with the SQLite database in order to create a durable SQL database and append records.

### [node-sqlite3](http://www.sqlitetutorial.net/sqlite-nodejs)
The [SQLite3](https://github.com/mapbox/node-sqlite3) Node.js module facilitates retrieval of database records to be represented visually on the frontend side, via the Express.js API, which performs SQL queries on the SQLite database. 

## Hosting Environment
### [Linode](https://www.linode.com)
A `Linode container` is an affordable virtual GNU/Linux web server environment hosted in the cloud. 
This project is hosted there in its entirety at the grand cost of only $5 per month.
The two Node.js projects that comprise this interactive web application include the Express.js project serving up API endpoints, and a React.js package to represent the queried data.
The least expensive service tier appears more than sufficient.
 `Linux version 4.16.7-1-ARCH`
 `1024MB RAM`
 `4096MB HDD`
 `1TB Data Cap`

### [Arch Linux](https://www.archlinux.org)
I used Arch for my Linode container because it is a minimalist-oriented GNU/Linux distribution with a robust support community (and superb documentation).
Arch Linux insists on early support of the most bleeding-edge new kernels and packages. This made it necessary to utilize Node's [npm n](https://www.npmjs.com/package/n) version management package to ensure compatibility with all the required packages of this project.
In particular, the `node-sqlite3` package does not yet provide binaries ready for `Node.js (version 10.0)` (provided by the Arch Linux package repository).
 ` n` allows for convenient access to the better-supported `Node.js (version 9.11.1)`.

### Security
Server hardening and other security considerations are extremely important when constructing a hosting environment.
Although this is an extensive topic, the basic considerations in this project were:
- Creating a non-root user account with restricted privileges and using sudo as a matter of course.
- Disabling remote log-in to the root account.
- Enforcing RSA-key authentication regime for SSH access.
- Disable and remove unnecessary daemons and services that listen actively on net ports.
- Configure iptables or other firewall tolerate only a narrow range of essential net traffic.
- Use fail2ban to lock out attackers after repeated failed attempts at login.
- ALWAYS keep software up to date with the latest security patches.

### Domain Name Registration
I registered the domain name yzfinance.org through the DNS registration service at [1and1.com](http://www.1and1.com)
Considerations were the cheap availability of a `.org` TLD for only $3.99, and that the 1&1 service allows for using Linode's own DNS Name Servers.
