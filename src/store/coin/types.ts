export interface MetaCoin {
    symbol_safe: string
    symbol_full: string
    symbol_icon_url: string
    name: string
}

export interface CoinAction {
    type: string
    payload: MetaCoin[]
}

export interface CoinState {
    coinList: MetaCoin[]
}
