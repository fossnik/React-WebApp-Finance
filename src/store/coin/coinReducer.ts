import {GET_LIST_OF_ALL_COINS} from '../index'
import {CoinAction, CoinState} from './types'

const initialState: CoinState = {
    coinList: [],
};

export const CoinReducer = (
    state: CoinState = initialState,
    action: CoinAction,
) => {
    switch (action.type) {
        case GET_LIST_OF_ALL_COINS:
            return {
                ...state,
                coinList: action.payload,
            };

        default:
            return state;
    }
};

