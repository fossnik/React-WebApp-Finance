import {API_URL} from '../../config'
import {GET_LIST_OF_ALL_COINS} from '../index'
import {CoinAction} from './types'

export const getListOfAllCoins = (): Promise<CoinAction> =>
    fetch(`${API_URL}/query_coin_index`)
        .then(res => res.json().then(json => res.ok ? json : Promise.reject(json)))
        .then(res => Promise.resolve({
            type: GET_LIST_OF_ALL_COINS,
            payload: res.coins,
        }))
        .catch(err => Promise.reject({'Could not Load from API': err}));
