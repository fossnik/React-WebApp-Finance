import {combineReducers} from 'redux'
import {CoinReducer} from './coin/coinReducer'
import {SnapReducer} from './snapshot/snapReducer'

export const GET_LIST_OF_ALL_COINS = 'GET_LIST_OF_ALL_COINS';
export const GET_ALL_SNAPSHOTS_FOR_COIN = 'GET_ALL_SNAPSHOTS_FOR_COIN';

export const rootReducer = combineReducers({
    CoinReducer,
    SnapReducer,
});

export type AppState = ReturnType<typeof rootReducer>;
