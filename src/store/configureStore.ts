import {applyMiddleware, compose, createStore} from 'redux'
import {rootReducer} from './index'
import promise from 'redux-promise'

declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
    }
}

const composeEnhancers =
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
      : compose;

export const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(promise))
);
