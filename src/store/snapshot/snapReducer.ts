import {GET_ALL_SNAPSHOTS_FOR_COIN} from '../index'
import {SnapAction, SnapState} from './types'

const initialState: SnapState = {
    snapIndex: {},
};

export const SnapReducer = (
    state: SnapState = initialState,
    action: SnapAction,
) => {
    switch (action.type) {
        case GET_ALL_SNAPSHOTS_FOR_COIN:
            return {
                ...state,
                snapIndex: {
                    ...state.snapIndex,
                    [action.payload.symbol_safe]: action.payload.snapshots,
                }
            };

        default:
            return state;
    }
};
