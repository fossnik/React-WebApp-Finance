const environment_var = require("./env.json")["API_URL"];
export const API_URL = window.location.hostname === "localhost"
	? "http://localhost:3000/api" // run locally
	: environment_var || "http://www.yzfinance.org:3000/api";
