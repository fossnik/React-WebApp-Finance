import React from 'react'
import '../index.css'
import StartButton from './StartButton'

const About = () =>
	<div className="HomePage">
		<div className="StartButton-container"><StartButton/></div>
		<div className="About-body">
			<h1 className="About-title">
				About Yz (wise) Finance
			</h1>

			<div className="About-contents">
				<div className="About-contents-box">
					<h3><a href="#javascript-frameworks">Node.js Frameworks</a></h3>
					<li>Express.js</li>
					<li>React.js & Redux.js</li>
					<li>React Router</li>
					<li>TypeScript</li>
				</div>

				<div className="About-contents-box">
					<h3><a href="#web-scraping">Web Scraping</a></h3>
					<li>Java Web Scraper (jsoup)</li>
					<li>Yahoo Finance</li>
				</div>

				<div className="About-contents-box">
					<h3><a href="#database">Database</a></h3>
					<li>SQLite</li>
					<li>SQLite-jdbc</li>
					<li>node-sqlite3</li>
				</div>

				<div className="About-contents-box">
					<h3><a href="#hosting-environment">Hosting Environment</a></h3>
					<li>Linode</li>
					<li>Arch Linux</li>
					<li>Security / Server Hardening</li>
					<li>Domain Name Registration</li>
				</div>
			</div>

			<article>
				<h2 id="javascript-frameworks">JavaScript Frameworks</h2>
				<h3><a href="https://www.nodejs.org">Node.js</a></h3>
				<p>
					<code>Node.JS</code> is a cross-platform JavaScript run-time environment that is versatile, and extremely popular. Many high-quality libraries are available through the npm ecosystem.
				</p>

				<h3><a href="https://www.reactjs.org">Express.js</a></h3>
				<p>
					<code>Express.js</code> is an event-driven HTTP framework often used for API. I use two independent Express instances - the frontend instance is a React website (yzFinance), which issues HTTP GET requests to the backend API (which itself is an Express.js instance with the ability to query the SQLite DB where my jSoup web-scrape data is persisted.)
				</p>

				<h3><a href="https://www.reactjs.org">React.js</a></h3>
				<p>
					<code>React.js</code> is a framework created by Facebook to facilitate more responsive UI; efficient state-driven web applications that spare server-side resources by offloading tasks to the client-side JavaScript interpreter.
					<br/>
					<code>JSX Syntax</code> is JavaScript syntax used to write React components.  It conveniently resembles the structure of HTML, but is not HTML; in fact, it is a shorthand for vanilla JavaScript functions that are significantly more verbose.
				</p>

				<h3><a href="https://redux.js.org">Redux.js</a></h3>
				<p>
					<code>Redux.js</code> - state management engine commonly paired with React.js.
				</p>

				<h3><a href="https://reacttraining.com/react-router">React Router</a></h3>
				<p>
					<code>React Router</code> is a Node.js package that implements navigation components and routing through a declarative programming model.
					This allows a unique URL to be associated to each object in my dataset.
				</p>

				<h3><a href="https://www.typescriptlang.org">TypeScript</a></h3>
				<p>
					<code>TypeScript</code> extends the JavaScript language, making it possible to explicitly define the shape of objects when they are declared, or passed into / returned from a function.
					Using TypeScript allowed me to define highly specific data objects and validate the construction of data passing within the program for total consistency.
				</p>
			</article>

			<article>
				<h2 id="web-scraping">Web Scraping</h2>
				<h3><a href="https://finance.yahoo.com/cryptocurrencies">YahooFinance</a></h3>
				<p>
					<code>Yahoo! Finance</code> is a free web resource and financial portfolio service from which the contents of the SQLite database are derived. It is public and reliable, although the implementation has tended to mutate on occasion.
					<code>finance.yahoo.com/cryptocurrencies</code>
				</p>

				<h3><a href="https://jsoup.org">jsoup</a></h3>
				<p>
					This project had originally used Selenium to perform web-scraping, but due to some difficulties with automating the chrome web driver to run headlessly on my Linode VM, it became clear that something simpler was preferable. Whereas Selenium is generally better suited to web testing automation because it emulates a full-fledged web browser, jsoup is extremely useful for simple data extraction.
					The <a href="https://gitlab.com/fossnik/JsoupWebScraper-yahFin">jsoup web scraper is available on GitLab</a>

					The Scraper runs autonomously as a Cron process on my Linode, committing market snapshots to my SQL database.
				</p>
			</article>

			<article>
				<h2 id="database">Database</h2>
				<h3><a href="https://www.sqlite.org">SQLite</a></h3>
				<p>
					<code>SQLite</code> is an open-source relational database management system that is one of most popular among a myriad of competing SQL-style database products. Although these are largely interchangeable, SQLite does appear to be a favorite in the world of Java and JavaScript because of it's active development, strong user-base, and vital support community.
				</p>

				<h3><a href="http://www.sqlitetutorial.net/sqlite-java/sqlite-jdbc-driver">SQLite-jdbc</a></h3>
				<p>
					The <code>SQLite-jdbc driver</code> for the Java Database Connector (JDBC) interfaces the Selenium web-scraper with the SQLite database in order to create a durable SQL database and append records.
				</p>

				<h3><a href="http://www.sqlitetutorial.net/sqlite-nodejs">node-sqlite3</a></h3>
				<p>
					The <a href="https://github.com/mapbox/node-sqlite3">SQLite3</a> Node.js module facilitates retrieval of database records to be represented visually on the frontend side, via the Express.js API, which performs SQL queries on the SQLite database.
				</p>
			</article>

			<article>
				<h2 id="hosting-environment">Hosting Environment</h2>
				<h3><a href="https://www.linode.com">Linode</a></h3>
				<p>
					A <code>Linode container</code> is an affordable virtual GNU/Linux web server environment hosted in the cloud.
					This project is hosted there in its entirety at the grand cost of only $5 per month.
					The two Node.js projects that comprise this interactive web application include the Express.js project serving up API endpoints, and a React.js package to represent the queried data.
					The least expensive service tier appears more than sufficient.
					<code>
						<li>Linux version 4.16.7-1-ARCH</li>
						<li>1024MB RAM</li>
						<li>4096MB HDD</li>
						<li>1TB Data Cap</li>
					</code>
				</p>

				<h3><a href="https://www.archlinux.org">Arch Linux</a></h3>
				<p>
					I used ArchLinux for my Linode container because it is a minimalist-oriented distro with a robust support community (and superb documentation).
					Arch Linux insists on early support of the most bleeding-edge new kernels and packages.
					This made it necessary to utilize Node's <a href="https://www.npmjs.com/package/n">npm n</a> version management package to ensure compatibility with all the required packages of this project.
					In particular, the <code>node-sqlite3</code> package does not yet provide binaries ready for <code>Node.js (version 10.0)</code> (provided by the Arch Linux package repository).
					<code>n</code> allows for convenient access to the better-supported <code>Node.js (version 9.11.1)</code>.
				</p>

				<h3>Security / System Hardening</h3>
				<p>
					Server hardening and other security considerations are extremely important when constructing a hosting environment.
					Although this is an extensive topic, the basic considerations in this project were:
					<li>Creating a non-root user account with restricted privileges and using sudo as a matter of course.</li>
					<li>Disabling remote log-in to the root account.</li>
					<li>Enforcing RSA-key authentication regime for SSH access.</li>
					<li>Disable and remove unnecessary daemons and services that listen actively on net ports.</li>
					<li>Configure iptables or other firewall tolerate only a narrow range of essential net traffic.</li>
					<li>Use fail2ban to lock out attackers after repeated failed attempts at login.</li>
					<li>ALWAYS keep software up to date with the latest security patches.</li>
				</p>

				<h3>Domain Name Registration</h3>
				<p>
					I registered the domain name <a href="http://www.yzfinance.org">yzfinance.org</a> through the DNS registration service at <a href="http://www.1and1.com">1and1.com</a><br/>
					Considerations were the cheap availability of a <code>.org</code> TLD for only $3.99, and that the 1&1 service allows for using Linode's own DNS Name Servers.
				</p>
			</article>
			<div className='PageEnd'/>
		</div>
	</div>;

export default About
