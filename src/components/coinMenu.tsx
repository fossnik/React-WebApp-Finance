import React from 'react'
import {MetaCoin} from '../store/coin/types'

export const CoinMenu = (props: {
    symbol_safe: string,
    coinList: MetaCoin[],
    history: {
        push: (path: string) => void,
    },
}) => (
    <div className='NaviBox-form'>
        <select
            value={props.symbol_safe}
            onChange={event => props.history.push(`/db/${event.target.value}`)}
            className='NaviBox-element'
        >
            {
                props.coinList.map((coin: MetaCoin) => (
                    <option value={coin.symbol_safe} key={coin.symbol_safe}>
                        [{coin.symbol_full}] {coin.name}
                    </option>
                ))
            }
        </select>
    </div>
);
