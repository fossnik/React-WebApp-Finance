import React from 'react'
import DatePicker from 'react-date-picker'
import _ from 'lodash'

import {CoinSnapshot} from '../store/snapshot/types'

const getDate = (dateTime: string): Date => {
    const dateString: string = dateTime.split(' ')[0];
    const [yyyy, mm, dd] = dateString.split('-');
    const local: Date = new Date(`${yyyy}-${mm}-${dd}`);
    const utc: Date = new Date(local.getTime() + local.getTimezoneOffset() * 60000);
    return utc;
};

export const SnapDatePicker = (props: {
    selectedSnapNum: number,
    snapshots: CoinSnapshot[],
    onSelectSnap: (snap: number) => void,
}): React.ReactElement<JSX.Element> => {
    const onDateChange = (date: Date) => {
        const yyyyMMdd: string = date.toISOString().split('T')[0];
        const snapMatch: CoinSnapshot | undefined = _.find(props.snapshots,
            (snap: CoinSnapshot) => snap.dateCreated.startsWith(yyyyMMdd));

        if (snapMatch) {
            props.onSelectSnap(snapMatch.ID);
        }
    };

    const snapMatch: CoinSnapshot | undefined = _.find(props.snapshots,
        (snap: CoinSnapshot) => snap.ID === props.selectedSnapNum);

    const selectedDate: Date | null = snapMatch ? getDate(snapMatch.dateCreated) : null;

    const minDate: Date = getDate(props.snapshots[0].dateCreated);
    const maxDate: Date = getDate(props.snapshots[props.snapshots.length - 1].dateCreated);

    if (selectedDate && minDate && maxDate) {
        return <div style={{
            padding: '14px'
        }}>
            <DatePicker
                value={selectedDate}
                format="y-M-d"
                returnValue='start'
                clearIcon={null}
                minDate={minDate}
                maxDate={maxDate}
                onChange={(date: Date | Date[]) => _.isDate(date) && onDateChange(date)}
            />
        </div>
    }

    return <></>;
};
