import React from 'react'
import {Provider} from 'react-redux'
import {BrowserRouter, Route, Switch} from 'react-router-dom'

import CoinIndex from './routes/CoinIndex'
import SnapshotIndex from './routes/SnapshotIndex'
import SnapshotDetail from './routes/SnapshotDetail'
import About from './components/About'

// redux
import {store} from './store/configureStore'

export default function App(): React.ReactElement {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <Switch>
                    <Route exact path='/db/:coin/:snapshot' component={SnapshotDetail}/>
                    <Route exact path='/db/:coin/' component={SnapshotIndex}/>
                    <Route exact path='/db/' component={CoinIndex}/>
                    <Route exact path='/' component={About}/>
                    <Route>{notFound}</Route>
                </Switch>
            </BrowserRouter>
        </Provider>
    )
}

const notFound = <h1 style={{
    textAlign: 'center',
    backgroundColor: 'white',
    margin: '20%',
    padding: '5%',
}}>
    Not Found
</h1>;
