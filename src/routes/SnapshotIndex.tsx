import React, {Component} from 'react'
import {connect} from 'react-redux'
import _ from 'lodash'

import {AppState} from '../store'
import {CoinAction, MetaCoin} from '../store/coin/types'
import {CoinSnapshot, SnapAction, SnapIndex} from '../store/snapshot/types'
import {getListOfAllCoins} from '../store/coin/coinActions'
import {getAllSnapsForCoin} from '../store/snapshot/snapActions'
import {CoinMenu} from '../components/coinMenu'

class SnapshotIndex extends Component <{
    getAllSnapsForCoin: (symbol_safe: string) => Promise<SnapAction>,
    getListOfAllCoins: () => Promise<CoinAction>,
    snapIndex: SnapIndex,
    coinList: MetaCoin[],
    history: {
        push: (path: string) => never,
    },
    match: {
        params: {
            coin: string,
        },
    },
}> {
    componentDidMount(): void {
        if (_.isEmpty(this.props.coinList)) {
            this.getCoins();
        }
    }

    private getCoins(): void {
        this.props.getListOfAllCoins()
            .then(() => void (0));
    }

    private getSnaps(symbol_safe: string): void {
        this.props.getAllSnapsForCoin(symbol_safe)
            .then(() => void (0));
    }

    onSelectSnap(snap: CoinSnapshot): void {
        this.props.history.push(`/db/${this.props.match.params.coin}/${snap.ID}`)
    }

    render(): React.ReactElement {
        const coinList: MetaCoin[] = this.props.coinList;
        const symbol_safe: string = this.props.match.params.coin;
        const snapshots: CoinSnapshot[] = this.props.snapIndex[symbol_safe];

        if (_.isUndefined(snapshots)) {
            this.getSnaps(symbol_safe);
        }
        else if (!_.isEmpty(coinList)) {
            const metaCoin = _.find(coinList, (coin: MetaCoin) =>
                coin.symbol_safe === symbol_safe);

            if (!metaCoin) {
                return <div className='Loading'>Coin Not Found</div>;
            }

            const icon = (
                <div style={{flex: 3}}>
                    <img className='SymbolIcon'
                         src={metaCoin.symbol_icon_url}
                         alt={metaCoin.symbol_full}
                         height='48'
                         width='48'
                    />
                </div>
            );

            return <React.Fragment>
                <CoinMenu
                    history={this.props.history}
                    symbol_safe={symbol_safe}
                    coinList={coinList}
                />
                <table className='Table'>
                    <thead className='Table-head'>
                    <tr>
                        <th className='Row-header-coin' colSpan={2}>
                            <div style={{
                                display: 'flex',
                                flexDirection: 'row',
                                alignItems: 'center',
                                minHeight: '175px',
                            }}>
                                {icon}
                                <div style={{
                                    flexDirection: 'column',
                                    flex: 2,
                                    justifyItems: 'space-evenly',
                                }}>
                                    <div style={{paddingBottom: '5px'}}>{metaCoin.name}</div>
                                    <div className='Header-subtitle'>{symbol_safe}</div>
                                </div>
                                {icon}
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <th>Web Scrape</th>
                        <th>Log Time (UTC)</th>
                    </tr>
                    </thead>
                    <tbody className='Table-body Click-able'>
                    {
                        _.orderBy(snapshots, ['ID'], ['desc'])
                            .map((snap: CoinSnapshot): JSX.Element => (
                                <tr
                                    key={snap.ID}
                                    onClick={() => this.onSelectSnap(snap)}
                                >
                                    <td id='r'>
                                        Scrape #{snap.ID}
                                    </td>
                                    <td>
                                        {new Date(snap.dateCreated).toLocaleString()}
                                    </td>
                                </tr>
                            ))
                    }
                    </tbody>
                </table>
            </React.Fragment>
        }

        return <div className='Loading'>Loading Snapshot Index...</div>
    }
}

const mapStateToProps = (state: AppState): {
    coinList: MetaCoin[],
    snapIndex: {},
} => ({
    coinList: state.CoinReducer.coinList,
    snapIndex: state.SnapReducer.snapIndex,
});

const mapDispatchToProps = (dispatch: any) => ({
    getAllSnapsForCoin: (symbol_safe: string) => dispatch(getAllSnapsForCoin(symbol_safe)),
    getListOfAllCoins: () => dispatch(getListOfAllCoins()),
});

export default connect(mapStateToProps, mapDispatchToProps)(SnapshotIndex)
