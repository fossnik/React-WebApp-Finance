import React, {Component} from 'react'
import {connect} from 'react-redux'

import {AppState} from '../store'
import {CoinAction, MetaCoin} from '../store/coin/types'
import {getListOfAllCoins} from '../store/coin/coinActions'

class CoinIndex extends Component<{
    getListOfAllCoins: () => Promise<CoinAction>,
    coinList: MetaCoin[],
    history: {
        push: (path: string) => never,
    },
}> {
    state = {
        loaded: false,
    };

    componentDidMount(): void {
        if (this.props.coinList.length > 0) {
            this.setState({loaded: true})
        } else {
            this.props.getListOfAllCoins()
                .then(() => this.setState({loaded: true}))
        }
    }

    onSelectCoin(symbol_safe: string): void {
        this.props.history.push(`/db/${symbol_safe}`)
    }

    render(): React.ReactElement {
        const coinList: JSX.Element[] = this.props.coinList
            .map((coin: MetaCoin) => (
                <tr
                    key={coin.symbol_safe}
                    onClick={() => this.onSelectCoin(coin.symbol_safe)}
                >
                    <td>{coin.symbol_full}</td>
                    <td>{coin.name}</td>
                </tr>
            ));

        return coinList.length > 0 ?
            <div className="Table-container">
                <table className="Table">
                    <thead className="Table-head">
                    <tr>
                        <th>Pair Symbol</th>
                        <th>Name</th>
                    </tr>
                    </thead>
                    <tbody className="Table-body Click-able">
                    {coinList}
                    </tbody>
                </table>
            </div>
            : <div className='Loading'>Loading Coin Index...</div>;
    }
}

const mapStateToProps = (state: AppState): {
    coinList: MetaCoin[],
} => ({
    coinList: state.CoinReducer.coinList,
});

const mapDispatchToProps = (dispatch: any) => ({
    getListOfAllCoins: () => dispatch(getListOfAllCoins()),
});

export default connect(mapStateToProps, mapDispatchToProps)(CoinIndex)
