import React, {Component} from 'react'
import {connect} from 'react-redux'
import _ from 'lodash'

import {AppState} from '../store'
import {CoinAction, MetaCoin} from '../store/coin/types'
import {CoinSnapshot, SnapAction, SnapIndex} from '../store/snapshot/types'
import {getListOfAllCoins} from '../store/coin/coinActions'
import {getAllSnapsForCoin} from '../store/snapshot/snapActions'
import {CoinMenu} from '../components/coinMenu'
import {SnapDatePicker} from '../components/SnapDatePicker'

class SnapshotDetail extends Component<{
    getAllSnapsForCoin: (symbol_safe: string) => Promise<SnapAction>,
    getListOfAllCoins: () => Promise<CoinAction>,
    snapIndex: SnapIndex,
    coinList: MetaCoin[],
    history: {
        push: (path: string) => never,
    },
    match: {
        params: {
            coin: string,
            snapshot: string,
        },
    },
}> {
    state = {
        symbol_safe: null,
        snap_number: null,
        coinList: [],
    };

    componentDidMount(): void {
        const symbol_safe = this.props.match.params.coin;

        if (this.props.snapIndex.hasOwnProperty(symbol_safe)) {
            this.setState({snapshot: this.props.snapIndex[symbol_safe]})
        } else {
            this.props.getAllSnapsForCoin(symbol_safe)
                .then((res: SnapAction) => this.setState({
                    snapshot: _.find(res.payload.snapshots, (snap: CoinSnapshot) => snap.ID === Number(this.props.match.params.snapshot)),
                }))
        }

        if (this.props.coinList.length > 0) {
            this.setState({coinList: this.props.coinList})
        } else {
            this.props.getListOfAllCoins()
                .then((res: CoinAction) => this.setState({
                    coinList: res.payload,
                }))
        }
    }

    static getDerivedStateFromProps(
        props: {
            match: {
                params: {
                    coin: string,
                    snapshot: string,
                },
            },
            snapIndex: SnapIndex,
        },
        state: {
            symbol_safe: string,
            snap_number: number,
        },
    ) {
        const param_coin = props.match.params.coin;
        const param_snapNum = Number(props.match.params.snapshot);
        const state_coin = state.symbol_safe;
        const state_snapNum = state.snap_number;

        if (state_coin === param_coin && param_snapNum === state_snapNum) return null;

        return {
            symbol_safe: param_coin,
            snap_number: param_snapNum,
        };
    }

    onSelectSnap(snap: number, history = this.props.history): void {
        history.push(`/db/${this.props.match.params.coin}/${snap}`)
    }

    render(): React.ReactElement {
        const loading = <div className='Loading'>Loading Detail View...</div>;
        const notFound = (item: string) => <div className='Loading'>{item} not Found</div>;

        if (_.isNull(this.state.snap_number) || _.isNull(this.state.symbol_safe)) return loading;

        if (_.isEmpty(this.state.coinList)) return loading;
        const metaCoin = _.find((this.props.coinList as MetaCoin[]),
            (coin: MetaCoin) => coin.symbol_safe === this.state.symbol_safe);

        if (_.isUndefined(metaCoin)) return notFound('Coin');
        const {name, symbol_full, symbol_safe, symbol_icon_url}: MetaCoin = metaCoin;

        if (_.isNull(this.state.snap_number)) return loading;
        const snapshots: CoinSnapshot[] = this.props.snapIndex[symbol_safe];
        const snapshot = _.find(snapshots, (snap: CoinSnapshot) =>
            snap.ID === (this.state.snap_number as unknown as number));

        if (_.isUndefined(snapshot)) return notFound('Snapshot');
        const {
            ID,
            change,
            circulatingSupply,
            dateCreated,
            marketCap,
            pChange,
            price,
            totalVolume24h,
            volume,
            volume24h,
        }: CoinSnapshot = snapshot;

        const prevSnap: number | null = _.find(snapshots, (snap: CoinSnapshot) =>
            snap.ID === ID - 1) ? (ID - 1) : null;

        const nextSnap: number | null = _.find(snapshots, (snap: CoinSnapshot) =>
            snap.ID === ID + 1) ? (ID + 1) : null;

        const icon = (
            <div style={{flex: 1, alignSelf: 'center'}}>
                <img className='SymbolIcon'
                     src={symbol_icon_url}
                     alt={symbol_full}
                     height='48'
                     width='48'
                />
            </div>
        );

        return <React.Fragment>
            <CoinMenu
                symbol_safe={symbol_safe}
                coinList={this.state.coinList}
                history={this.props.history}
            />
            <div className='Detail-coin' style={{
                display: 'flex',
                flexDirection: 'row',
            }}>
                {icon}
                <div style={{
                    display: 'flex',
                    flexDirection: 'column',
                }}>
                    <div className='FullName'>{name}</div>
                    <div className='Symbol'>{symbol_full}</div>
                    <div className='Date'>{(new Date(dateCreated)).toLocaleString()}</div>
                    <div className='SnapShot'>Snapshot {ID}</div>
                </div>
                {icon}
            </div>
            <div className='dateNav'>
                <button onClick={() => this.onSelectSnap(ID - 1)}
                        className='snapNavButton'
                        disabled={!prevSnap}>
                    ⬅
                </button>
                <SnapDatePicker
                    onSelectSnap={(snap: number) => this.onSelectSnap(snap, this.props.history)}
                    snapshots={snapshots}
                    selectedSnapNum={this.state.snap_number as unknown as number}
                />
                <button onClick={() => this.onSelectSnap(ID + 1)}
                        className='snapNavButton'
                        disabled={!nextSnap}>
                    ➡
                </button>
            </div>
            <table className='Table Table-container'>
                <tbody className='Table-body'>
                <tr>
                    <td id='r'>Price</td>
                    <td>$ {price.toLocaleString()}</td>
                </tr>
                <tr>
                    <td id='r'>Change</td>
                    <td>
                        {
                            change === 0 ? <span>0</span> : change < 0 ?
                                <span className='Arrow-down'>
                                    $ {change.toLocaleString()} ▼ {pChange.toLocaleString()}%
                                </span>
                                : <span className='Arrow-up'>
                                    $ {change.toLocaleString()} ▲ {pChange.toLocaleString()}%
                                </span>
                        }
                    </td>
                </tr>
                <tr>
                    <td id='r'>Market Cap</td>
                    <td>{marketCap.toLocaleString()}</td>
                </tr>
                <tr>
                    <td id='r'>Volume</td>
                    <td>{volume.toLocaleString()}</td>
                </tr>
                <tr>
                    <td id='r'>Volume 24h</td>
                    <td>{volume24h.toLocaleString()}</td>
                </tr>
                <tr>
                    <td id='r'>Total Volume 24h</td>
                    <td>{totalVolume24h.toLocaleString()}</td>
                </tr>
                <tr>
                    <td id='r'>Circulating Suppy</td>
                    <td>{circulatingSupply.toLocaleString()}</td>
                </tr>
                </tbody>
            </table>
        </React.Fragment>
    }
}

const mapStateToProps = (state: AppState): {
    coinList: MetaCoin[],
    snapIndex: SnapIndex,
} => ({
    coinList: state.CoinReducer.coinList,
    snapIndex: state.SnapReducer.snapIndex,
});

const mapDispatchToProps = (dispatch: any) => ({
    getAllSnapsForCoin: (symbol_safe: string) => dispatch(getAllSnapsForCoin(symbol_safe)),
    getListOfAllCoins: () => dispatch(getListOfAllCoins()),
});

export default connect(mapStateToProps, mapDispatchToProps)(SnapshotDetail)
