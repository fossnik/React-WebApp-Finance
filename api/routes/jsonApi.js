const express = require('express');
const router = express.Router();
const sqlite3 = require('sqlite3').verbose();
const DB_PATH = require('../config.js')['DB_PATH'];
let counter = 0;

/* GET /api/query_coin/ endpoint */
router.get('/query_coin/:currencyPair/', allSnapshotsOfPair);

function allSnapshotsOfPair(req, res, next) {
	const db = new sqlite3.Database(DB_PATH, sqlite3.OPEN_READONLY, (err) => {
		if (err) {
			console.error(err.message);
			return next(err);
		}
	});

	const coin = req.params.currencyPair;
	let sql = `SELECT *
	           FROM _all_your_coin
	           NATURAL JOIN ${coin}`;

	if (!isNaN(req.headers.limit)) {
		sql += ` LIMIT ${req.headers.limit}`;
	}

	db.all(sql, [], (err, snapshots) => {
		if (err) {
			console.error(err.message);
			return next(err);
		}

		const {
			name,
			symbol_safe,
			symbol_full,
			symbol_icon_url,
		} = snapshots[0];

		res.json({
			name,
			symbol_safe,
			symbol_full,
			symbol_icon_url,
			snapshots: snapshots.map(snapshot => {
				return {
					ID: snapshot.ID,
					dateCreated: snapshot.dateCreated,
					price: snapshot.price,
					change: snapshot.change,
					pChange: snapshot.pChange,
					marketCap: snapshot.marketCap,
					volume: snapshot.volume,
					volume24h: snapshot.volume24h,
					totalVolume24h: snapshot.totalVolume24h,
					circulatingSupply: snapshot.circulatingSupply,
				}
			})
		});

		console.log(`Passed back ${snapshots.length}${!isNaN(req.headers.limit) ? ' (LIMITED QUERY)' : ''} snapshots of ${coin}`);
	});

	db.close((err) => {
		if (err) {
			console.error(err.message);
			return next(err);
		}
	});
}

/* GET /api/query_coin_index endpoint */
router.get('/query_coin_index/', coinsIndex);

function coinsIndex(req, res, next) {
	const db = new sqlite3.Database(DB_PATH, sqlite3.OPEN_READONLY, (err) => {
		if (err)
			console.error(err.message);

		console.log(` [${++counter}]\tJSON API <=> Database @ ${new Date().toUTCString()}`);
	});

	// acquire list of all coins
	const sql = `SELECT symbol_safe, symbol_full, symbol_icon_url, name
	             FROM _all_your_coin
	             ORDER BY symbol_safe`;
	db.all(sql, [], (err, coins) => {
		if (err)
			return next(err);

		const visible_coins = require('./config.coin_whitelist')['symbols'];

		// return JSON array of ONLY coins found in whitelist
		coins = coins.filter(coin => visible_coins.includes(coin.symbol_safe));
		res.json({coins});
		console.log(`Returned JSON object of ${coins.length} coin(s)`);
	});

	db.close((err) => {
		if (err)
			console.error(err.message);
	});
}

module.exports = router;
