const environment_var = require("./env.json")["DB_PATH"];
const DB_PATH = environment_var || "SnapshotDB-dev.sqlite3";

module.exports = {
	DB_PATH
};
