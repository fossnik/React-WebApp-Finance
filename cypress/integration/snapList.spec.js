/// <reference types="Cypress" />

describe('/db/adausd page is valid - Cardano USD', () => {
	it('header contains "Cardano USD"', () => {
		cy.visit('/db/adausd')
		cy.get('th.Row-header-coin')
			.should($header => expect($header).to.contain('Cardano USD'))
	})

	it('table contains a link to /db/adausd/99', () => {
		cy.visit('/db/adausd')
		cy.get('tbody.Table-body')
			.should($table => expect($table).to.contain('Scrape #99'))
	})

	it('click on "Scrape #99" navigates to /db/adausd/99', () => {
		cy.visit('/db/adausd')
		cy.get('tbody.Table-body')
			.contains('Scrape #99')
			.click()

		cy.url().should('eq', Cypress.config().baseUrl + '/db/adausd/99')

		cy.get('div.FullName')
			.should($div => expect($div).to.contain('Cardano USD'))
	})
})

describe('/db/ltcusd page is valid - Litecoin USD', () => {
	it('header contains "Litecoin USD"', () => {
		cy.visit('/db/ltcusd')
		cy.get('th.Row-header-coin')
			.should($header => expect($header).to.contain('Litecoin USD'))
	})

	it('table contains a link to /db/ltcusd/99', () => {
		cy.visit('/db/ltcusd')
		cy.get('tbody.Table-body')
			.should($table => expect($table).to.contain('Scrape #99'))
	})

	it('click on "Scrape #99" navigates to /db/ltcusd/99', () => {
		cy.visit('/db/ltcusd')
		cy.get('tbody.Table-body')
			.contains('Scrape #99')
			.click()

		cy.url().should('eq', Cypress.config().baseUrl + '/db/ltcusd/99')

		cy.get('div.FullName')
			.should($div => expect($div).to.contain('Litecoin USD'))
	})
})
