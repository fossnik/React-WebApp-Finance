/// <reference types="Cypress" />

describe('Displays page correctly', () => {
	it('displays table headers', () => {
		cy.visit('/db/')
		cy.get('table.Table')
			.children('thead.Table-head')
			.children('tr')
			.should($tr => {
				expect($tr[0].innerHTML).eq('<th>Pair Symbol</th><th>Name</th>')
			})
	})

	it('has coin elements', () => {
		cy.visit('/db/')
		cy.get('tbody.Table-body')
			.should($tbody => {
				expect($tbody).to.contain('ADA-USD')
				expect($tbody).to.contain('Cardano USD')
				expect($tbody).to.contain('ZRX-USD')
				expect($tbody).to.contain('0x USD')
			})
	})
})

describe('Links to coin pages on click', () => {
	beforeEach(() => {
		cy.visit('/db/')
	})

	it('links to ada page', () => {
		cy.get('tbody.Table-body')
			.contains('ADA-USD')
			.click()

		cy.url().should('eq', Cypress.config().baseUrl + '/db/adausd')

		cy.get('div.Header-subtitle')
			.should($div => {
				expect($div).to.contain('adausd')
			})
	})

	it('links to zrx page', () => {
		cy.get('tbody.Table-body')
			.contains('ZRX-USD')
			.click()

		cy.url().should('eq', Cypress.config().baseUrl + '/db/zrxusd')

		cy.get('div.Header-subtitle')
			.should($div => {
				expect($div).to.contain('zrxusd')
			})
	})
})
