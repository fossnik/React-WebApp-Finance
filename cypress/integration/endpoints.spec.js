/// <reference types="Cypress" />

describe('Web Endpoints', () => {
	it('loads Web index of coins on localhost', () => {
		cy.visit('http://127.0.0.1:3000/web')
		cy.get('h1')
			.should(($h1) => {
				expect($h1).to.contain('Index of Coins')
			})
	})

	it('redirects from top level route to /web', () => {
		cy.visit('http://127.0.0.1:3000/')
		cy.location('pathname').should('eq', '/web')
	})
})

describe('api/query_coin_index endpoint contains correct data', () => {
	it('loads /query_coin_index endpoint', () => {
		cy.request('http://127.0.0.1:3000/api/query_coin_index').as('coins')
		// .then(res => cy.writeFile('cypress/fixtures/endpoints/query_coin_index.json', res.body))
		cy.get('@coins').should(res => {
			expect(res.body).to.have.property('coins')
			expect(res.body).property('coins').length.gt(100)
		})
		cy.get('@coins').should(res => {
			expect(res.body).to.deep.equals(require('../fixtures/endpoints/query_coin_index'))
		})
	})
})

describe('api/query_coin/ endpoint contains correct data', () => {
	it('loads adausd coin', () => {
		cy.request('http://127.0.0.1:3000/api/query_coin/adausd').as('adausd')
		// .then(res => cy.writeFile('cypress/fixtures/endpoints/query_coin/adausd.json', res.body))

		cy.get('@adausd').should(res => {
			expect(res.body).to.deep.equals(require('../fixtures/endpoints/query_coin/adausd.json'))
		})
	})

	it('loads btcusd coin', () => {
		cy.request('http://127.0.0.1:3000/api/query_coin/btcusd').as('btcusd')
		// .then(res => cy.writeFile('cypress/fixtures/endpoints/query_coin/btcusd.json', res.body))

		cy.get('@btcusd').should(res => {
			expect(res.body).to.deep.equals(require('../fixtures/endpoints/query_coin/btcusd.json'))
		})
	})
})
