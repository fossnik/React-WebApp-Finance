/// <reference types="Cypress" />

describe('Date elements agree', () => {
	it('has ', () => {
		cy.visit('/db/btcusd/1')
		cy.get('.react-date-picker__inputGroup > input:nth-child(1)')
			.then($input => {
				const datePickerDate = $input[0].value;
				const [yyyy, mm, dd] = datePickerDate.split('-').map(s=>Number(s));
				cy.get('.Date').contains(`${mm}/${dd}/${yyyy}`)
			})
	})
})
